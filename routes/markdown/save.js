'use strict';

const Boom = require('boom');
const Joi = require('joi');

module.exports = {
  method: 'POST',
  path: '/markdown/save',
  handler: function(request, reply) {
    let db = request.server.plugins['hapi-mongodb'].db;
    let ObjectID = request.server.plugins['hapi-mongodb'].ObjectID;
    let convert = request.server.plugins['md2html'].convert;

    let html = convert(request.payload.markdown);
    let item = {
      markdown: request.payload.markdown,
      html: html
    }

    db.collection('notes').insertOne(item, (err, result) => {
      if (err) return Boom.internal('Something bad just happened. Please try again later!');

      reply({
        id: result.insertedId,
        html: html
      });
    });
  },
  config: {
    validate: {
      payload: {
        markdown: Joi.string().required()
      }
    }
  }
}
