'use strict';

const Boom = require('boom');
const Joi = require('joi');

module.exports = {
  method: 'GET',
  path: '/markdown/get/{id}',
  handler: function(request, reply) {
    let db = request.server.plugins['hapi-mongodb'].db;
    let ObjectID = request.server.plugins['hapi-mongodb'].ObjectID;

    let id = request.params.id;
    
    if (!ObjectID.isValid(id)) {
      return reply(Boom.badRequest('Incorrect note id'));
    }

    db.collection('notes').findOne({ '_id': new ObjectID(request.params.id) }, (err, item) => {
      if (err) return reply(Boom.internal('Something bad happened, please try again later!'));
      if (!item) return reply(Boom.notFound('Note is not found'));

      reply(item);
    });
  }
}
