'use strict';

const PATTERNS = {
  header: /^(#{1,6})(.+)$/,
  link: /\[([\w\s\-\_]+)\]\((.+)\)/g,
  strong: /(\*{2}(.+?)\*{2})/g,
  em: /(\*([^\*]+)\*)/g
};

exports.register = function (server, options, next) {

  server.expose('convert', function(source) {
    let result = [];
    let match;
    let lines = source.split(/\n+/g);

    for (let line of lines) {
      if ( (match = line.match(PATTERNS.header)) !== null) {
        let headerLevel = match[1].length;

        line = line.replace(PATTERNS.header, `<h${headerLevel}>$2</h${headerLevel}>`);
      } else {
        line = `<p>${line}</p>`;
      }

      line = line.replace(PATTERNS.link, '<a href="$2">$1</a>');
      line = line.replace(PATTERNS.strong, '<strong>$2</strong>');
      line = line.replace(PATTERNS.em, '<em>$2</em>');

      result.push(line);
    }

    return result.join('');
  });

  next();
};

exports.register.attributes = {
  name: 'md2html',
  verstion: '0.0.1'
}
