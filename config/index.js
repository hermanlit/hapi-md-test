'use strict';

const config = {
  development: {
    plugins: {
      mongodb: {
        url: process.env.MONGO_URI || 'mongodb://test:test@ds011228.mlab.com:11228/mdnotes', //|| 'mongodb://127.0.0.1:27017/mdnotes',
        settings: {}
      }
    }
  },
  test: {
    plugins: {
      mongodb: {
        url: process.env.MONGO_URI || 'mongodb://test:test@ds015934.mlab.com:15934/mdnotes_test', //|| 'mongodb://127.0.0.1:27017/mdnotes_test',
        settings: {}
      }
    }
  },
  production: {

  }
};

module.exports = config[process.env.NODE_ENV || 'development'];
