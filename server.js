'use strict';

const Hapi = require('hapi');
const glob = require('glob');
const path = require('path');
const config = require('./config');

const server = new Hapi.Server();

server.connection({ port: process.env.PORT || 3000 });

const plugins = [
  { register: require('hapi-mongodb'), options: config.plugins.mongodb },
  { register: require('./plugins/md2html') }
];

server.register(plugins, (err) => {
  if (err) throw err;

  glob.sync('routes/**/*.js', { root: __dirname }).forEach(filename => {
    let routeConfig = require(path.resolve(__dirname, filename));
    server.route(routeConfig);
  });

  server.emit('initialized');

  if (!module.parent) {
    server.start((err) => {
      if (err) throw err;

      console.log(`Running at ${server.info.uri}`);
    });
  }
});

module.exports = server;
