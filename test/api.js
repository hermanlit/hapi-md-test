'use strict';

const Lab = require('lab');
const expect = require('code').expect;

const lab = exports.lab = Lab.script();
const server = require('../server');

lab.experiment('Markdown Endpoint', () => {
  let createdItemId;

  lab.before((done) => {
    server.on('initialized', done);
  });

  lab.test('returns 400 when payload is empty', (done) => {
    let options = {
      method: 'POST',
      url: '/markdown/save'
    };

    server.inject(options, (response) => {
      expect(response.statusCode).to.equal(400);
      done();
    });
  })

  lab.test('return ObjectID when request is successful', (done) => {
    let options = {
      method: 'POST',
      url: '/markdown/save',
      payload: {
        markdown: '##Header\nLorem ipsum dolor sit amet, **consectetur *adipiscing* elit. Mauris id diam justo.**\nMauris vulputate, nisl ac efficitur fermentum, lacus nisi tincidunt nunc, at rutrum est libero ac neque.'
      }
    };

    server.inject(options, (response) => {
      let result = response.result;

      createdItemId = result.id.toString();
      expect(response.statusCode).to.equal(200);
      expect(result.id).to.exist();
      expect(result.html).to.exist();
      expect(createdItemId.length).to.equal(24);
      done();
    });
  })

  lab.test('returns 400 on malformed id param', (done) => {
    let options = {
      method: 'GET',
      url: '/markdown/get/test'
    };

    server.inject(options, (response) => {
      expect(response.statusCode).to.equal(400);
      done();
    });
  })

  lab.test('returns 404 on wrong id param', (done) => {
    let wrongId = '576246f505e4d73c33333333';
    let options = {
      method: 'GET',
      url: '/markdown/get/' + wrongId
    };

    server.inject(options, (response) => {
      expect(response.statusCode).to.equal(404);
      done();
    });
  })

  lab.test('return note when request is successful', (done) => {
    let options = {
      method: 'GET',
      url: '/markdown/get/' + createdItemId
    };

    server.inject(options, (response) => {
      expect(response.statusCode).to.equal(200);
      expect(response.result).to.exist();
      expect(response.result.markdown).to.exist();
      expect(response.result.html).to.exist();
      done();
    });
  })
})
